public class indexedset extends set {
    public indexedset() {
	super();
    }

    //@ requires 0 <= index && index < count();
    //@ ensures \result == ((Integer) l.get(index)).intValue();
    public /*@ pure @*/ int getElement(int index) {
	return ((Integer) l.get(index)).intValue();
    }

    //@ requires 0 <= index && index < count();
    //@ ensures (\forall int i;0 <= i && i < index;l.get(i).equals(\old(l.get(i))));
    //@ ensures (\forall int i;index <= i && i < count();l.get(i).equals(\old(l.get(i + 1))));
    //@ ensures \old(count()) - 1 == count();
    public void removeElementByIdx(int index) {
	l.remove(index);
	count--;
    }

    //@ requires 0 <= idx && idx < count();
    //@ requires !hasElement(e);
    //@ ensures hasElement(e);
    //@ ensures \old(count()) + 1 == count();
    //@ ensures (\forall int i;0 <= i && i < idx;l.get(i).equals(\old(l.get(i))));
    //@ ensures (\forall int i;idx <= i && i < count() - 1;l.get(i + 1).equals(\old(l.get(i))));
    public void insertElement(int e, int idx) {
	l.add(idx, new Integer(e));
    }

    //@ also
    //@ ensures (\forall int i;0 <= i && i < count() - 1;l.get(i).equals(\old(l.get(i))));
    public void addElement(int e) {
	super.addElement(e);
    }

    //@ also
    //@ ensures (\forall int i;0 <= i && i < l.indexOf(new Integer(e));l.get(i).equals(\old(l.get(i))));
    //@ ensures (\forall int i;l.indexOf(new Integer(e)) <= i && i < count();l.get(i).equals(\old(l.get(i + 1))));
    public void removeElement(int e) {
	super.removeElement(e);
    }
}