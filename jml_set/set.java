import java.util.List;
import java.util.ArrayList;

public class set {
    protected /*@ spec_public @*/ int count;
    protected /*@ spec_public @*/ List l;

    //@ invariant count >= 0;
    //@ invariant l != null;

    //@ initially count == 0;
    public set() {
	count = 0;
	l = new ArrayList();
    }

    //@ ensures \result == count;
    public /*@ pure @*/ int count() {
	return count;
    }

    //@ requires !hasElement(e);
    //@ ensures hasElement(e);
    //@ ensures \old(count()) + 1 == count();
    public void addElement(int e) {
	l.add(new Integer(e));
	count++;
    }

    //@ requires count() > 0;
    //@ requires hasElement(e);
    //@ ensures !hasElement(e);
    //@ ensures \old(count()) - 1 == count();
    public void removeElement(int e) {
	l.remove(new Integer(e));
	count--;
    }

    //@ ensures l.contains(new Integer(e)) == \result;
    public /*@ pure @*/ boolean hasElement(int e) {
	return l.contains(new Integer(e));
    }
}