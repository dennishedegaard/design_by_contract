#!/usr/bin/python3
#
# A simple converter for bin <-> dec <-> hex implemented
# in python3.

""" Converts a positive number into a hexadecimal value. """
def dec_to_hex(n):
	result = []
	while (n > 0):
		# get the modulus and append
		mod = n % 16
		if mod == 15:
			result.append('f')
		elif mod == 14:
			result.append('e')
		elif mod == 13:
			result.append('d')
		elif mod == 12:
			result.append('c')
		elif mod == 11:
			result.append('b')
		elif mod == 10:
			result.append('a')
		else:
			result.append(mod)
		n = int(n / 16)
	# reverse the list
	result.reverse()
	s = "0x"
	# append the list elements to a string
	for n in result:
		s += str(n)
	return s

""" convers a hexadecimal value into a decimal value. """
def hex_to_dec(n):
	if (n.startswith('0x')):
		n = n[2:] # remove the 0x prefix, if found.
	expo = 0
	result = 0
	while (len(n) > 0):
		# get the last char
		char = n[-1]
		num = 0
		# convert it
		if (char == 'a' or char == 'A'):
			num = 10
		elif (char == 'b' or char == 'B'):
			num = 11
		elif (char == 'c' or char == 'C'):
			num = 12
		elif (char == 'd' or char == 'D'):
			num = 13
		elif (char == 'e' or char == 'E'):
			num = 14
		elif (char == 'f' or char == 'F'):
			num = 15
		else:
			num = int(char)
		# append and increment exponent
		result += num * 16 ** expo
		expo+=1
		# remove last digit
		n = n[:-1]
	return result

""" converts the decimal number n into a binary number. """
def dec_to_bin(n):
	result = ""
	while (n > 0):
		mod = n % 2
		n = int(n / 2)
		result = str(mod) + result
	if len(result) == 0:
		return '0'
	return result


""" converts the binary number n into decimal. """
def bin_to_dec(n):
	expo = 0
	result = 0
	while (len(n) > 0):
		# get the last char
		num = int(n[-1])
		# append and increment exponent
		result += num * 2 ** expo
		expo+=1
		# remove last digit
		n = n[:-1]
	return result

if __name__ == '__main__':
	# 101 to binary
	print("101 to binary:")
	print('\t', dec_to_bin(101), sep='')
	# 0x1234 to dec
	print("0x1234 to dec:")
	print('\t', hex_to_dec('0x1234'), sep='')
	# 00001010101111001101 to hex
	print("00001010101111001101 to hex:")
	print('\t', dec_to_hex(bin_to_dec('00001010101111001101')), sep='')
	# fe:fd:00:00:5c:a4 to decimal
	print("fe:fd:00:00:5c:a4 to decimal:")
	print('\t', hex_to_dec('fe'), ':',
			hex_to_dec('fd'), ':',
			hex_to_dec('00'), ':',
			hex_to_dec('00'), ':',
			hex_to_dec('5c'), ':',
			hex_to_dec('a4'), sep='') 
	# 131.247.168.48 to binary
	print("131.247.168.48 to binary:");
	print('\t', dec_to_bin(131), '.',
			dec_to_bin(247), '.',
			dec_to_bin(168), '.',
			dec_to_bin(48), sep='');
	# 131.247.168.48 to hex
	print("131.247.168.48 to hex:");
	print('\t', dec_to_hex(131), '.',
			dec_to_hex(247), '.',
			dec_to_hex(168), '.',
			dec_to_hex(48), sep='');
	# 255.255.128.0 to bin
	print("255.255.128.0 to bin:");
	print('\t', dec_to_bin(255), '.',
			dec_to_bin(255), '.',
			dec_to_bin(128), '.',
			dec_to_bin(0), sep='');
