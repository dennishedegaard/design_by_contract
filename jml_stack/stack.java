import java.util.LinkedList;

public class stack{
    private /*@ spec_public @*/ LinkedList l;
    private /*@ spec_public @*/ int count;

    //@ invariant l != null;
    //@ invariant count() >= 0;

    // command
    //@ initially count() == 0;
    public stack() {
	l = new LinkedList();
	count = 0;
    }

    // basic
    //@ ensures (\forall int i;0 <= i && i < count(); ((Integer) l.get(i)).intValue() == \result[i]);
    public /*@ pure @*/ int[] getAllElements() {
	int[] result = new int[count()];
	//@ loop_invariant 0 <= i && i < count();
	//@ increasing:
	for (int i = 0;i < count();i++)
	    result[i] = ((Integer) l.get(i)).intValue();
	return result;
    }

    // derived
    //@ requires count() > 0;
    //@ ensures \result == getAllElements()[count() - 1];
    public /*@ pure @*/ int read() {
	return getAllElements()[count() - 1];
    }

    // (derived) command
    //@ requires count() > 0;
    //@ ensures count() + 1 == \old(count());
    //@ ensures (\forall int i;0 <= i && i < count();getAllElements()[i] == \old(getAllElements())[i]);
    public void remove() {
	pop();
    }

    // basic
    //@ ensures \result == count;
    public /*@ pure @*/ int count() {
	return count;
    }

    // command
    //@ ensures count() == \old(count()) + 1;
    //@ ensures getAllElements()[count() - 1] == e;
    //@ ensures (\forall int i;0 <= i && i < count() - 1;getAllElements()[i] == \old(getAllElements())[i]);
    public void push(int e) {
	l.addLast(new Integer(e));
	count++;
    }

    // command
    //@ requires count() > 0;
    //@ ensures count() + 1 == \old(count());
    //@ ensures \result == \old(read());
    //@ ensures (\forall int i;0 <= i && i < count();getAllElements()[i] == \old(getAllElements())[i]);
    public int pop() {
	count--;
	return ((Integer) l.removeLast()).intValue();
    }
}