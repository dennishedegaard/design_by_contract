public class StackNode {
    private /*@ spec_public nullable @*/ Object element;
    private /*@ spec_public nullable @*/ StackNode prev = null;
    private /*@ spec_public nullable @*/ StackNode next = null;

    public StackNode(Object element) {
	this.element = element;
    }

    public /*@ pure @*/ Object getElement() {
	return element;
    }

    public void setElement(Object element) {
	this.element = element;
    }

    public /*@ pure @*/ StackNode getPrev(int i) {
	if (i == 0)
	    return this;
	else
	    return this.getPrev(i - 1);
    }

    public /*@ pure @*/ StackNode getPrev() {
	return prev;
    }

    public void setPrev(StackNode prev) {
	this.prev = prev;
    }

    public /*@ pure @*/ StackNode getNext() {
	return next;
    }

    public void setNext(StackNode next) {
	this.next = next;
    }
}
