public class Test {
    public static void main(String[] args) {
	Stack s = new Stack(100);
	{
	    int i = 0;
	    while (!s.isFull()) {
		System.out.println("Adding " + i);
		s.put(new Integer(i));
		i++;
	    }
	}
	while (!s.isEmpty()) {
	    System.out.println(s.get(s.getSize() - 1));
	    s.remove();
	}
    }
}
