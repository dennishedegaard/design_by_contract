public class Stack {
    private /*@ spec_public @*/ int size;
    private /*@ spec_public nullable @*/ StackNode head = null;
    private /*@ spec_public nullable @*/ StackNode tail = null;
    private /*@ spec_public @*/ int capacity;

    //@ invariant size >= 0;

    //@ requires capacity >= 0;
    //@ ensures size == 0;
    //@ ensures capacity == this.capacity;
    public Stack(int capacity) {
	size = 0;
	this.capacity = capacity;
    }

    //@ ensures \result == size;
    public /*@ pure @*/ int getSize() {
	return size;
    }

    //@ ensures (size == 0) == \result;
    public /*@ pure @*/ boolean isEmpty() {
    	return size == 0;
    }

    //@ ensures (size == capacity) == \result;
    public /*@ pure @*/ boolean isFull() {
	return size == capacity;
    }

    //@ requires idx >= 0;
    //@ requires idx < size;
    //@ ensures \result != null;
    // ensures \result == head.getPrev(idx).getElement();
    public /*@ pure @*/ Object get(int idx) {
	StackNode ptr = head;
	while (idx-- > 0)
	    ptr = ptr.getNext();
	return ptr.getElement();
    }

    //@ requires size < capacity;
    //@ ensures size == \old(size) + 1;
    //@ ensures tail.getElement() == ele;
    public void put(Object ele) {
	if (head == null) {
	    head = new StackNode(ele);
	    tail = head;
	} else {
	    StackNode newNode = new StackNode(ele);
	    tail.setNext(newNode);
	    newNode.setPrev(tail);
	    tail = newNode;
	}
	size++;
    }

    //@ ensures tail == null ==> \result == null;
    //@ ensures tail != null ==> \result == tail.getElement();
    public /*@ pure @*/ Object peek() {
	if (tail == null)
	    return null;
	else
	    return tail.getElement();
    }

    //@ requires size > 0;
    //@ ensures peek().equals(element);
    public void replace(Object element) {
	tail.setElement(element);
    }

    //@ requires size > 0;
    //@ requires tail.getPrev() != null;
    //@ ensures size == \old(size) - 1;
    public void remove() {
	if (head == tail) {
	    head = null;
	    tail = null;
	} else {
	    StackNode ptr = tail.getPrev();
	    System.out.println(ptr);
	    ptr.setNext(null);
	    tail.setPrev(null);
	    tail = ptr;
	}
	size--;
    }
}
