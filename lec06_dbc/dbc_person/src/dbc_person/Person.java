package dbc_person;

import java.util.Calendar;

public class Person {
	public static int GENDER_MALE = 1;
	public static int GENDER_FEMALE = 2;

	private String /* @ spec_public @ */name;
	private Calendar /* @ spec_public @ */birth;
	private Calendar /* @ spec_public @ */death;
	private int /* @ spec_public @ */gender;
	private Person /* @ spec_public @ */father;
	private Person /* @ spec_public @ */mother;

	/* @invariant gender == GENDER_MALE && gender == GENDER_FEMALE; @ */

	/* @requires name != null; @ */
	/* @requires birth != null; @ */
	public Person(String name, Calendar birth, Calendar death, int gender) {
		assert name != null : "name is null";
		assert name.length() > 0 : "name is empty";
		assert birth != null : "birth is null";
		assert gender == GENDER_FEMALE || gender == GENDER_MALE : "gender not male or female";
		assert death == null ? true : death.after(birth) : "death before birth";
		this.name = name;
		this.birth = birth;
		this.death = death;
		this.gender = gender;
	}

	public Person getFather() {
		return father;
	}

	/* @requires father != null; @ */
	/* @requires father.birth.before(birth); @ */
	/* @requires father.gender == GENDER_MALE; @ */
	public void setFather(Person father) {
		assert father != null : "father is null";
		assert father.birth.before(birth) : "father birth after birth";
		assert father.gender == GENDER_MALE : "father gender not male";
		this.father = father;
	}

	public Person getMother() {
		return mother;
	}

	/* @requires mother.birth.before(birth); @ */
	/* @requires mother.gender GENDER_FEMALE; @ */
	public void setMother(Person mother) {
		assert mother.birth.before(birth) : "mothers birth after birth";
		assert mother.gender == GENDER_FEMALE : "mother gender not female";
		this.mother = mother;
	}

	public String getName() {
		return name;
	}

	/* @requires name != null; @ */
	/* @requires name.length() > 0; @ */
	public void setName(String name) {
		assert name != null : "name is null";
		assert name.length() > 0 : "name length is 0";
		this.name = name;
	}

	public Calendar getBirth() {
		return birth;
	}

	public Calendar getDeath() {
		return death;
	}

	/* @requires death == null ? true : death.after(birth); @ */
	public void setDeath(Calendar death) {
		assert death == null ? true : death.after(birth) : "death before birth";
		this.death = death;
	}

	public int getGender() {
		return gender;
	}

	public void setGender(int gender) {
		assert gender == GENDER_FEMALE || gender == GENDER_MALE : "gender not male or female";
		this.gender = gender;
	}
}
