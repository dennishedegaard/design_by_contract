package dbc_person;

import java.util.GregorianCalendar;

public class Test {
	public static void main(String[] args) {
		Person p = new Person("svend", new GregorianCalendar(1234, 12, 12),
				null, Person.GENDER_MALE);
		try {
		p.setDeath(new GregorianCalendar(1234, 11, 11));
		} catch (AssertionError e) {
			assert e.getMessage().equals("death before birth");
		}
		System.out.println(p.toString());
	}
}
