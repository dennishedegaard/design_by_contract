public final class qsort {
    private qsort() {}

    //@ requires arr != null;
    //@ ensures (\forall int i; 0 <= i && i < arr.length - 1; arr[i] <= arr[i + 1]);
    public static void qsort(int arr[]) {
	_qsort(arr, 0, arr.length - 1);
    }

    //@ requires arr != null;
    //@ ensures (\forall int i; left <= i && i < right; arr[i] <= arr[i + 1]);
    private static void _qsort(int arr[], int left, int right) {
	// This is part of B, not {P}.
	if (right > left) {
	    // Attempt to set pivot in the middle.
	    int pivotIndex = left + (right - left) / 2;
	    int pivotNewPivot = partition(arr, left, right, pivotIndex);
	    _qsort(arr, left, pivotNewPivot - 1);
	    _qsort(arr, pivotNewPivot + 1, right);
	}
    }

    //@ requires arr != null;
    //@ requires arr.length > 0;
    //@ requires right > left;
    //@ requires left <= pivotIndex && pivotIndex <= right;
    //@ ensures (\forall int i; i >= left && i < \result; arr[\result] >= arr[i]);
    //@ ensures (\forall int i; i > \result && i <= right; arr[\result] <= arr[i]);
    private static int partition(int arr[], int left, int right, int pivotIndex) {
	int pivotValue = arr[pivotIndex];
	swap(arr, pivotIndex, right);
	int storeIndex = left;
	//@ loop_invariant i >= left && i <= right;
	//@ loop_invariant (\forall int j;j >= left && j < storeIndex; pivotValue >= arr[j]);
	//@ loop_invariant (\forall int j;j > storeIndex && j < i; pivotValue <= arr[j]);
	//@ increasing:
	for (int i = left;i < right;i++)
	    if (arr[i] < pivotValue) {
		swap(arr, i, storeIndex);
		storeIndex++;
	    }
	swap(arr, storeIndex, right);
	return storeIndex;
    }

    //@ requires arr != null;
    //@ requires 0 <= idx1 && idx1 < arr.length;
    //@ requires 0 <= idx2 && idx2 < arr.length;
    //@ ensures \old(arr[idx1]) == arr[idx2];
    //@ ensures \old(arr[idx2]) == arr[idx1];
    private static void swap(int arr[], int idx1, int idx2) {
	int tmp = arr[idx2];
	arr[idx2] = arr[idx1];
	arr[idx1] = tmp;
    }
}