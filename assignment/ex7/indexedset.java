import java.util.*;

public final class indexedset {
    private /*@ spec_public @*/ List l;

    //@ invariant l != null;
    //@ invariant (\forall int i; i >= 0 && i < size(); (\forall int j; j >= 0 && j < size(); i != j ==> !get(i).equals(get(j))));

    //@ initially l != null;
    public indexedset() {
      	l = new ArrayList();
    }

    //@ requires o != null;
    //@ requires (\forall int i;0 <= i && i < size(); !get(i).equals(o));
    //@ assignable l;
    //@ ensures \old(size()) + 1 == size();
    //@ ensures \old(isEmpty()) ==> !isEmpty();
    //@ ensures (\forall int i;0 <= i && i < \old(size()); get(i).equals(\old(get(i))));
    public void insert(Object o) {
	l.add(o);
    }

    //@ requires 0 <= idx && idx < size();
    //@ ensures \result.equals(l.get(idx));
    public /*@ pure @*/ Object get(int idx) {
	return l.get(idx);
    }

    //@ requires 0 <= idx && idx < size();
    //@ requires !isEmpty();
    //@ assignable l;
    //@ ensures \old(size()) - 1 == size();
    //@ ensures (\forall int i; 0 <= i && i < idx - 1; get(i).equals(\old(get(i))));
    //@ ensures (\forall int i; idx <= i && i < \old(size()) - 1; get(i).equals(\old(get(i + 1))));
    public void remove(int idx) {
	l.remove(idx);
    }

    //@ requires o != null;
    //@ requires indexOf(o) != -1;
    //@ requires !isEmpty();
    //@ assignable l;
    //@ ensures \old(size()) - 1 == size();
    //@ ensures (\forall int i; 0 <= i && i < \old(indexOf(o)) - 1; get(i).equals(\old(get(i))));
    //@ ensures (\forall int i; \old(indexOf(o)) <= i && i < \old(size()) - 1; get(i).equals(\old(get(i + 1))));
    public void remove(Object o) {
	l.remove(o);
    }

    //@ requires o != null;
    //@ requires l.contains(o);
    //@ ensures \result == l.indexOf(o);
    public /*@ pure @*/ int indexOf(Object o) {
	return l.indexOf(o);
    }

    //@ ensures \result == l.size();
    public /*@ pure @*/ int size() {
	return l.size();
    }

    //@ ensures \result == (size() == 0);
    public /*@ pure @*/ boolean isEmpty() {
	return size() == 0;
    }
}