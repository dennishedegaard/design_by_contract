public class exercise1 {
    public static int bin_to_dec(String s) {
	assert s.length() <= 16;
	int result = 0;
	int expo = 0;
	while (s.length() > 0) {
	    int num = s.charAt(s.length() - 1) - '0';
	    result += num * Math.pow(2, expo);
	    expo++;
	    s = s.substring(0, s.length() - 1);
	}
	return result;
    }

    public static String dec_to_bin(int i) {
	StringBuffer result = new StringBuffer();
	while (i > 0) {
	    int mod = i % 2;
	    i /= 2;
	    result.insert(0, mod);
	}
	return result.toString();
    }

    public static void main(String[] args) {
	assert parse_char('4') == 4;
	assert parse_char('f') == 15;
	assert parse_char('D') == 13;
	assert bin_to_dec("101010") == 42;
	assert bin_to_dec("0000000000101010") == 42;
       	assert dec_to_bin(42).equals("101010");
    }
}