import java.util.Calendar;

public abstract class Person {
    private /*@ spec_public @*/ String name;
    private /*@ spec_public @*/ Calendar birth;
    private /*@ spec_public nullable @*/ Person father;
    private /*@ spec_public nullable @*/ Person mother;
    private /*@ spec_public @*/ int sequencenumber;

    //@ initially name != null;
    //@ initially birth != null;

    //@ requires name != null;
    //@ requires birth != null;
    //@ requires sequencenumber >= 0 && sequencenumber <= 9999;
    //@ requires cprCheck(parseCpr(birth, sequencenumber));
    //@ assignable this.name, this.birth, this.sequencenumber;
    public Person(String name, Calendar birth, int sequencenumber) {
	this.name = name;
	this.birth = birth;
	this.sequencenumber = sequencenumber;
    }

    //@ ensures \result == father;
    public /*@ pure @*/ Person getFather() {
	return father;
    }

    //@ requires father != null;
    //@ requires father.birth.before(birth);
    //@ assignable this.father;
    //@ ensures father != null ==> father == this.father;
    public void setFather(Person father) {
	this.father = father;
    }

    //@ ensures \result == mother;
    public /*@ pure @*/ Person getMother() {
	return mother;
    }

    //@ requires mother != null;
    //@ requires mother.birth.before(birth);
    //@ assignable this.mother;
    //@ ensures mother != null ==> mother == this.mother;
    public void setMother(Person mother) {
	this.mother = mother;
    }

    //@ ensures \result == name;
    public /*@ pure @*/ String getName() {
	return name;
    }

    //@ requires name != null;
    //@ assignable this.name;
    //@ ensures name != null ==> name == this.name;
    public void setName(String name) {
	this.name = name;
    }

    //@ ensures \result == birth;
    public /*@ pure @*/ Calendar getBirth() {
	return birth;
    }

    //@ ensures \result != null;
    public abstract /*@ pure @*/ String getGender();

    protected /*@ spec_public @*/ final int[] multi = { 4, 3, 2, 7, 6, 5, 4, 3, 2, 1 };

    public /*@ pure @*/ static long parseCpr(Calendar birth, int sequencenumber) {
	StringBuffer buffer = new StringBuffer();
	{
	    int i = birth.get(Calendar.DAY_OF_MONTH);
	    String s = String.valueOf(i);
	    buffer.append(i < 0 ? "0" + s : s);
	}
	{
	    int i = birth.get(Calendar.MONTH) + 1;
	    String s = String.valueOf(i);
	    buffer.append(i < 0 ? "0" + s : s);
	}
	{
	    int i = birth.get(Calendar.YEAR);
	    String s = String.valueOf(i);
	    while (s.length() < 4)
		s = "0" + s;
	    buffer.append(s);
	}
	{
	    String s = String.valueOf(sequencenumber);
	    while (s.length() < 4)
		s = "0" + s;
	    buffer.append(s);
	}
	return Long.valueOf(buffer.toString()).longValue();
    }

    //@ ensures cpr >= 101000000 && cpr <= 3112999999L;
    public /*@ pure @*/ static boolean cprCheck(long cpr) {
	int[] digits = new int[10];
	{
	    char[] _digits = String.valueOf(cpr).toCharArray();
	    for (int i = 0;i < _digits.length;i++)
		digits[i] = _digits[i] - '0';
	}
	//	System.out.println(java.util.Arrays.toString(digits));
	return false;
    }
}
