import java.util.Calendar;

public class FemalePerson extends Person {
    //@ requires sequencenumber % 2 == 0;
    public FemalePerson(String name, Calendar birth, int sequencenumber) {
	super(name, birth, sequencenumber);
    }

    //@ also
    //@ ensures \result.equals("Female");
    public String getGender() {
	return "Female";
    }
}