import java.util.Calendar;

public class MalePerson extends Person {
    //@ requires (sequencenumber % 2) == 1;
    public MalePerson(String name, Calendar birth, int sequencenumber) {
	super(name, birth, sequencenumber);
    }

    //@ also
    //@ ensures \result.equals("Male");
    public String getGender() {
	return "Male";
    }
}