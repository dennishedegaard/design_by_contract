import java.util.Arrays;

/**
 * A simple Queue implemented using a circular buffer.
 *
 * @author Dennis Hedegaard
 */
public class Queue {
    private /*@ spec_public @*/ int count;
    private /*@ spec_public non_null @*/ Object[] queue;
    private /*@ spec_public @*/ int head;
    private /*@ spec_public @*/ int tail;

    //@ invariant count() >= 0;
    //@ invariant count() <= capacity();

    //@ initially size() == 0;
    //@ initially queue.length == capacity();

    //@ constraint queue == \old(queue);

    //@ requires capacity > 0;
    public Queue(int capacity) {
	this.count = 0;
	this.queue = new Object[capacity];
	this.head = 0;
	this.tail = -1;
    }
    
    //@ requires size() < capacity();
    //@ ensures items()[count() - 1].equals(e);
    //@ ensures count() == \old(count()) + 1;
    /*@ ensures !is_empty() ==> (\forall int i;
      0 <= i && i < \old(size() - 1);
      items()[i].equals(\old(items()[i])));
      @*/
    public void put(Object e) {
	tail = (tail + 1) % capacity();
	queue[tail] = e;
	count++;
    }

    //@ requires size() > 0;
    //@ ensures size() > 0 ==> \result.equals(queue[head]);
    public /*@ pure @*/ Object head() {
	return queue[head];
    }

    // ensures \result == TODO
    public /*@ pure @*/ int size() {
	if (head > tail)
	    return tail + (queue.length - head) + 1;
	else
	    return tail - head + 1;
    }

    //@ requires !is_empty();
    //@ ensures count() == \old(count()) - 1;
    //@ ensures Arrays.equals(\old(tail()), items());
    /*@ ensures !is_empty() ==> (\forall int i;
      0 <= i && i < \old(items().length - 1);
      items()[i].equals(\old(items()[i + 1])));
      @*/
    public void remove() {
	queue[head] = null;
	head = (head + 1) % queue.length;
	count--;
    }

    //@ ensures \result == (count() == 0);
    public /*@ pure @*/ boolean is_empty() {
	return count == 0;
    }

    //@ ensures \result == (count() == capacity());
    public /*@ pure @*/ boolean is_full() {
	return count == capacity();
    }

    //@ ensures \result == count;
    public /*@ pure @*/ int count() {
	return count;
    }

    //@ ensures \result == queue.length;
    public /*@ pure @*/ int capacity() {
	return queue.length;
    }

    //@ ensures size() == 0;
    public void clear() {
	while (!is_empty())
	    remove();
    }

    //@ requires q != null;
    //@ ensures q != null && size() > 0 ==> \result == (size() == q.size());
    /*@ ensures q != null && size() == q.size() ==> (\forall int i;
      0 <= i && i < size();
      items()[i].equals(q.items()[i]));
      @*/
    public /*@ pure @*/ boolean is_equal(Queue q) {
	if (q.size() != size())
	    return false;
	Object[] t_it = items();
	Object[] q_it = q.items();
	for (int i = 0;i < size();i++)
	    if (!t_it[i].equals(q_it[i]))
		return false;
	return true;
    }

    //@ requires count() > 0;
    //@ ensures \result.length == count();
    public /*@ pure @*/ Object[] items() {
	Object[] items = new Object[count];
	if (head > tail) {
	    int i = 0;
	    for (int j = head;j < capacity();j++)
		items[i++] = queue[j];
	    for (int j = 0;j <= tail;j++)
		items[i++] = queue[j];
	} else {
	    int i = 0;
	    for (int j = head;j <= tail;j++)
		items[i++] = queue[j];
	}
	return items;
    }

    //@ requires count() > 1;
    //@ ensures \result.length == count() - 1;
    public /*@ pure @*/ Object[] tail() {
	Object[] items = new Object[count - 1];
	if (head > tail) {
	    int i = 0;
	    for (int j = head + 1;j < capacity();j++)
		items[i++] = queue[j];
	    for (int j = 0;j <= tail;j++)
		items[i++] = queue[j];
	} else {
	    int i = 0;
	    for (int j = head + 1;j <= tail;j++)
		items[i++] = queue[j];
	}
	return items;
    }
}