import java.util.Arrays;

public class Test {
    public static void main(String[] args) {
	Queue q = new Queue(10);
	for (int i = 0;i < 10;i++) {
	    q.put(new Integer(i));
	    System.out.println(arraysToString(q.items()) + " {" + q.size() + "}");
	}
	q.remove();
	System.out.println(arraysToString(q.items()) + " {" + q.size() + "}");
	q.remove();
	System.out.println(arraysToString(q.items()) + " {" + q.size() + "}");
	q.put("svend");
	System.out.println(arraysToString(q.items()) + " {" + q.size() + "}");
	q.put("lars");
	System.out.println(arraysToString(q.items()) + " {" + q.size() + "}");
	if (!q.is_full())
	    q.put("henning");
    }

    public static String arraysToString(Object[] array) {
	if (array == null)
	    return "[null]";
	else if (array.length == 0)
	    return "[]";
	StringBuffer buffer = new StringBuffer();
	buffer.append("[");
	for (int i = 0;i < array.length;i++)
	    if (i + 1 == array.length)
		buffer.append(array[i].toString());
	    else
		buffer.append(array[i].toString() + ", ");
	buffer.append("]");
	return buffer.toString();
    }
}